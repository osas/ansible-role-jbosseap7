Jboss EAP 7
===========

This role was previously maintained by:
* Ding-Yi Chen
* Timo Trinks

Now this role is in maintenance mode only. It is used to deploy Zanata.


This role will install and configure Jboss EAP standalone. Currently only
"standlone" mode is supported. All configuratuon changes are done through
jboss-cli under the hood.

If mysql role is detected, ( mysql_enabled: True), then it will automatically
install  mysql-connector-java and deploy it.

Requirements
------------

Ansible 1.9 or higher
Red Hat Enterprise Linux
Valid Red Hat Subscription for Jboss EAP 6

Role Variables
--------------

Currently the following variables are supported:

  ## General ==

  # Which config file to use (without .xml extension)
  jbosseap7_jboss_server_config: 'standalone'

  # Start EAP with clean/initial config and apply all changes against it.
  jbosseap7_clean_config: 'false'

  # Home dir of EAP installation
  jbosseap7_jboss_home: '/usr/share/jbossas'

  # Path of config dir
  jbosseap7_jboss_config_dir: "{{jbosseap7_jboss_home}}/standalone/configuration"

  # EAP will run as this user and group
  jbosseap7_jboss_user: 'jboss'
  jbosseap7_jboss_group: 'jboss'

  # Path to store .cli generated scripts
  jbosseap7_cli_batch_dir: "{{jbosseap7_jboss_config_dir}}/cli_batch"

  # Need to know all files to clean when clean_config = true
  jbosseap7_cli_files:
    - filename: 'datasources.cli'
    - filename: 'system-properties.cli'
    - filename: 'logging.cli'
    - filename: 'vault.cli'
    - filename: 'web.cli'

  ## end of General ==

  # System Properties, <system-properties></system-properties>
  jbosseap7_system_properties:
    - name: 'prop1'
      value: 'prop1val1'

  # Logging, <subsystem xmlns="urn:jboss:domain:logging">
  jbosseap7_logging:
    logger:
      - category: 'com.foo' # required
        level: 'DEBUG'      # required
    formatter:
      - name: 'myformatter' # required
        pattern: 'XXX'      # required
    custom_handler:
      - name: 'myhandler'   # required
        class: 'com.bla'    # required
        module: 'com.aa'    # required
        formantter: 'XXX'
        level: 'XXX'
        properties: '("name1" => "val1"),("name2" => "val2")'

  # Web, <subsystem xmlns="urn:jboss:domain:web">
  jbosseap7_web:
    valve:
      - name: 'myvalve'    # required
        module: 'com.bla'  # required
        class: 'com.bla'   # required
        params: '("name1" => "val1"),("name2" => "val2")'

#  # Vault, <vault></vault>
#  vault:
#    keystore_url: '/path/vault.keystore'
#    keystore_password: 'MASK-***'
#    keystore_alias: 'vault'
#    salt: 'XXXXX'
#    iteration_count: 'XX'
#    enc_file_dir: '/path/'

  # JDBC drivers
  jbosseap7_jdbc_drivers:
    - file: 'file1.jar'

  # Datasources, <subsystem xmlns="urn:jboss:domain:datasources">
  jbosseap7_datasources:
    - DS:
      name: 'MyDS'
      connection_url: 'jdbc:mysql://localhost:3306/dbname'
      connection_properties:
        - name: 'prop1'
          value: 'val1'
      jndi_name: 'java:jboss/datasources/MyDS'
      driver_name: 'mysql.jdbc.Driver_5_1'
      jta: 'true'
      connectable: 'true'
      use_java_context: 'true'
      spy: 'true'
      use_ccm: 'true'
      statistics_enabled: 'true'
      driver_class: 'com.mydriver.jdbc.Driver'
      datasource_class: 'com.datasoure.class'
      new_connection_sql: 'select 1'
      transaction_isolation: 'TRANSACTION_XX'
      url_delimiter: 'XXX'
      url_selector_strategy_class_name: 'XXX'
      pool:
        min_pool_size: 'XXX'
        max_pool_size: 'XXX'
        prefil: 'true'
        use_strict_min: 'true'
        flush_strategy: 'XXX'
        allow_multiple_users: 'true'
      security:
        user_name: 'XXX'
        password: "{{ db_password }}"
        security_domain: 'XXX'
        reauth_plugin_class_name: 'XXX'
        reauth_plugin_properties: 'XXX'
      validation:
        valid_connection_checker_class_name: 'XXX'
        valid_connection_checker_properties: 'XXX'
        check_valid_connection_sql: 'XXX'
        validate_on_match: 'true'
        background_validation: 'false'
        background_validation_millis: 'XXX'
        use_fast_fail: 'true'
        stale_connection_checker_class_name: 'XXX'
        stale_connection_checker_properties: 'XXX'
        exception_sorter_class_name: 'XXX'
        exception_sorter_properties: 'XXX'
      timeout:
        blocking_timeout_millis: 'XXX'
        idle_timeout_minutes: 'XXX'
        set_tx_query_timeout: 'XXX'
        query_timeout: 'XXX'
        use_try_lock: 'XXX'
        allocation_retry: 'XXX'
        allocation_retry_wait_millis: 'XXX'
      statement:
        track_statements: 'true'
        prepared_statement_cache_size: 'XXX'
        share_prepared_statements: 'true'

Dependencies
------------

None

Example Playbook
----------------

  roles:
    - role: 'jbosseap7'
      jbosseap7_jdbc_drivers:
        - file: 'mysql-connector-java-5.1.37-bin.jar'
        - file: 'postgresql-9.4.1207.jar'
      jbosseap7_datasources:
        - borg:
          name: 'MysqlDS'
          connection_url: 'jdbc:mysql://localhost:3306/borg'
          connection_properties:
            - name: 'autoReconnect'
              value: 'true'
            - name: 'useUnicode'
              value: 'true'
            - name: 'characterEncoding'
              value: 'UTF-8'
            - name: 'maxReconnects'
              value: '999999'
          jndi_name: 'java:jboss/datasources/MysqlDS'
          driver_name:
'mysql-connector-java-5.1.37-bin.jarcom.mysql.jdbc.Driver_5_1'
          driver_class: 'com.mysql.jdbc.Driver'
          use_java_context: 'true'
          transaction_isolation: 'TRANSACTION_READ_COMMITTED'
          pool:
            min_pool_size: '10'
            max_pool_size: '100'
            prefill: 'true'
          security:
            user_name: 'borg'
            password: 'blabla'
          validation:
            validate_on_match: 'true'
            valid_connection_checker_class_name:
'org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker'
            exception_sorter_class_name:
'org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter'
          statement:
            prepared_statement_cache_size: '32'
            share_prepared_statements: 'true'

Author Information
------------------

Vladimir Vasilev
